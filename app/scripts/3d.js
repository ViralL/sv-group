var control,camera,scene,renderer,axes,width,height;
width=window.innerWidth;
height=window.innerHeight;
// start();
// animate();


var localRenderObject = function() {

    // переменная для текстуры
    var bitmap = new Image();
    bitmap.src = 'images/jupiter.jpg'; // предзагрузка текстуры
    bitmap.onerror = function () {
        console.error("Error loading: " + bitmap.src);
    }

    // создаем сцену, камеру и рендер
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45 + ((1900 - width) / 100), width / height, 0.1, 80);
    //camera = new THREE.OrthographicCamera(width / - 20, width / 20, height / 20, height / - 20, 0.1, 1000);
    renderer = new THREE.WebGLRenderer( { alpha: true } ); // прозрачный фон
    renderer.setClearColor(0x000000, 0); // цвет фона
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true; // инициализируем тень

    // создаем оси


    function addHelperLine()
    {
        var counter = -50;
        while(counter < 50)
        {
            var material = new THREE.LineBasicMaterial({
                color: 0x0000ff
            });

            var geometry = new THREE.Geometry();
            geometry.vertices.push(
                new THREE.Vector3( -50, counter, 0 ),
                new THREE.Vector3( 50, counter, 0 )
            );

            var line = new THREE.Line( geometry, material );
            scene.add( line );

            var geometry = new THREE.Geometry();
            geometry.vertices.push(
                new THREE.Vector3( counter, -50, 0 ),
                new THREE.Vector3( counter, 50, 0 )
            );

            var line = new THREE.Line( geometry, material );
            scene.add( line );
            counter++;
        }

        var axisHelper = new THREE.AxisHelper( 50 );
        scene.add( axisHelper );
    }

    //addHelperLine()

    // создаем плоскость
    var planeGeometry = new THREE.PlaneGeometry(60,20,1,1); // размер плоскости
    var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff}); // цвет плоскости
    var plane = new THREE.Mesh(planeGeometry,planeMaterial);
    //plane.rotation.x = -0.5 * Math.PI; // поворачиваем сцену на 90градусов
    //plane.position.x = 10;
    plane.position.x = 0;
    plane.position.y = 0;
    plane.position.z = 0;
    plane.receiveShadow = true; // добавляем тень
    // scene.add(plane); // добавляем плоскость в сцену

    // создаем освещение
    var spotLight = new THREE.SpotLight( 0xffffff);
    spotLight.position.set( -70, -60, 30 );
    spotLight.castShadow = true; // добавляем тень
    scene.add(spotLight ); // добавляем освещение в сцену

    // создаем группу мини-точек освещения
    var lights = [];
    lights[0] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[1] = new THREE.PointLight( 0xffffff, 1, 0 );
    lights[2] = new THREE.PointLight( 0xffffff, 1, 0 );

    lights[0].position.set( 0, 200, 0 );
    lights[1].position.set( 100, 200, 100 );
    lights[2].position.set( -100, -200, -100 );

    scene.add( lights[0] );
    scene.add( lights[1] );
    scene.add( lights[2] );

    // направляем камеру
    camera.position.x = 0;
    camera.position.y = 0;
    camera.position.z = 50;
    camera.lookAt(new THREE.Vector3(0,0,0)); // смотрит прямо на сцену


    // добавляем на сайт
    document.getElementById("WebGL-output").appendChild(renderer.domElement);



    // задаем основной цвет для объектов
    // var meshMaterial = new THREE.MeshLambertMaterial({color: 0x7777ff});
    var wireFrameMat = new THREE.MeshBasicMaterial();
    wireFrameMat.wireframe = true;



    // текстурный материал для объекта
    var jupiter   = THREE.ImageUtils.loadTexture('images/jupiter.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var cloud    = THREE.ImageUtils.loadTexture('images/cloud.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var box       = THREE.ImageUtils.loadTexture('images/box.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var brick     = THREE.ImageUtils.loadTexture('images/brick.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var metall   = THREE.ImageUtils.loadTexture('images/metall.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var water    = THREE.ImageUtils.loadTexture('images/water.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var wood    = THREE.ImageUtils.loadTexture('images/wood.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    var meshMaterial = new THREE.MeshPhongMaterial({ map: water });
    var materialsFace = [jupiter, cloud, box, brick, metall, water, wood, cloud];
    var meshFaceMaterial = new THREE.MeshFaceMaterial( materialsFace );
    // сфера
    var texture = THREE.ImageUtils.loadTexture('images/1.jpg', {}, function() {
        renderer.render(scene, camera);
    });
    /*var material = new THREE.MeshPhongMaterial({ map: texture});
     var sphereGeometry = new THREE.SphereGeometry(4, 20, 20);
     var sphere = new THREE.Mesh(sphereGeometry, material);
     sphere.position.x = 0;
     sphere.position.y = 0;
     sphere.position.z = 20;
     scene.add(sphere);*/


    // задаем грани и вершины многоугольника
    var normal = new THREE.Vector3( 0, 1, 0 );
    var color = new THREE.Color( 0xffaa00 );
    var step=0;
    var vertices = [
        new THREE.Vector3(6,0,0),
        new THREE.Vector3(-6,0,0),
        new THREE.Vector3(0,5,0),
        new THREE.Vector3(0,-12,0),
        new THREE.Vector3(0,0,6),
        new THREE.Vector3(0,0,-6)
    ];
    var faces = [
        new THREE.Face3(0,2,4),
        new THREE.Face3(0,4,3),
        new THREE.Face3(0,3,5),
        new THREE.Face3(0,5,2),
        new THREE.Face3(1,2,5),
        new THREE.Face3(1,5,3),
        new THREE.Face3(1,3,4),
        new THREE.Face3(1,4,2),
    ];


    // создаем многоугольник
    var geom = new THREE.Geometry();
    geom.vertices = vertices;
    geom.faces = faces;
    geom.computeFaceNormals();

    var bricks = [
        new THREE.Vector2(0, 0),
        new THREE.Vector2(1, 0),
        new THREE.Vector2(1, 1),
        new THREE.Vector2(0, 1)
    ];

    geom.faceVertexUvs[0][0] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][1] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][2] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][3] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][4] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][5] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][6] = [ bricks[0], bricks[1], bricks[2] ];
    geom.faceVertexUvs[0][7] = [ bricks[0], bricks[1], bricks[2] ];

    // how many times to repeat in each direction; the default is (1,1),
    //   which is probably why your example wasn't working
    var triangleMaterial = new THREE.MeshPhongMaterial({
        map: texture
    });
    var triangleMaterial1 = new THREE.MeshPhongMaterial({
        map: wood
    });


    // задаем цвета для каждой грани многоугольника
    var mats = [];
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    mats.push(triangleMaterial);
    //mats.push(new THREE.MeshLambertMaterial({ color: 0xff00ff }));


    //var meshTexture = [];
    /* meshTexture.push(new THREE.ImageUtils.loadTexture('images/jupiter.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/cloud.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/box.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/brick.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/metall.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/water.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/wood.jpg'));
     meshTexture.push(new THREE.ImageUtils.loadTexture('images/box.jpg'));*/

    // var meshTexture = [
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/jupiter.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/cloud.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/box.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/brick.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/metall.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/water.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/wood.jpg')
    //      }),
    //      new THREE.MeshLambertMaterial({
    //          map: THREE.ImageUtils.loadTexture('images/box.jpg')
    //      })
    //   ];

    // var meshFaceMaterial = new THREE.MeshFaceMaterial( meshTexture );
    // var cube = new THREE.Mesh(new THREE.CubeGeometry(10, 10,10), meshFaceMaterial);
    // cube.overdraw = true;
    // cube.position.x = 5;
    // cube.position.y = 5;
    // cube.position.z = 0;
    // scene.add(cube);

    // задаем цвета для каждой грани многоугольника
    var l = geom.faces.length;
    for( var i = 0; i < l; i ++ ) {
        var j =+ i;
        geom.faces[ j ].materialIndex = i % 8;
        //console.log(j);
    }


    // добавляем многоугольник в сцену
    //var meshColor = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    // meshColor.position.x = -5;
    //meshColor.position.y = 5;
    //meshColor.position.z = 0;
    //meshColor.castShadow = true; // добавляем тень объекту
    //scene.add( meshColor );
    var scale = [];
    scale[1] = {x: 1.25, y: 1.65, z: 1.45};
    scale[2] = {x: 1.75, y: 2, z: 1.65};
    scale[3] = {x: 1.5, y: 2, z: 1.5};
    scale[4] = {x: 0.8, y: 0.95, z: 0.9};
    scale[0] = {x: 1.1, y: 1.1*1.3, z: 1.1};
    /////
    var polygon = {};
    polygon[0] = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    polygon[1] = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    polygon[2] = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    polygon[3] = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    polygon[4] = new THREE.Mesh( geom, new THREE.MeshFaceMaterial( mats ) );
    polygon[0].position.x = 2;
    polygon[0].position.y = 2;
    polygon[0].position.z = 8;
    polygon[0].rotation.x = -0.1;
    polygon[0].rotation.y = 0.3;
    polygon[0].rotation.z = 0.45;
    polygon[0].castShadow = true; // добавляем тень объекту
    polygon[0].scale.set(scale[0].x, scale[0].y, scale[0].z);
    scene.add( polygon[0] );
    polygon[1].position.x = 18;
    polygon[1].position.y = 0;
    polygon[1].position.z = -17;
    polygon[1].rotation.x = -0.05;
    polygon[1].rotation.y = 0.05;
    polygon[1].rotation.z = 2.59;
    polygon[1].castShadow = true; // добавляем тень объекту
    polygon[1].scale.set(scale[1].x, scale[1].y, scale[1].z);
    scene.add( polygon[1] );
    polygon[2].position.x = -16;
    polygon[2].position.y = -3;
    polygon[2].position.z = -9;
    polygon[2].rotation.x = -0.4;
    polygon[2].rotation.y = 0.2;
    polygon[2].rotation.z = 3.9;
    polygon[2].castShadow = true; // добавляем тень объекту
    polygon[2].scale.set(scale[2].x, scale[2].y, scale[2].z);
    scene.add( polygon[2] );
    polygon[3].position.x = -6;
    polygon[3].position.y = -5;
    polygon[3].position.z = -2;
    polygon[3].rotation.x = 0;
    polygon[3].rotation.y = 0.2;
    polygon[3].rotation.z = 2.8;
    polygon[3].castShadow = true; // добавляем тень объекту
    polygon[3].scale.set(scale[3].x, scale[3].y, scale[3].z);
    scene.add( polygon[3] );
    polygon[4].position.x = 19;
    polygon[4].position.y = -9;
    polygon[4].position.z = -5;
    polygon[4].rotation.x = 0.2;
    polygon[4].rotation.y = -0.6;
    polygon[4].rotation.z = 4.1;
    polygon[4].castShadow = true; // добавляем тень объекту
    polygon[4].scale.set(scale[4].x, scale[4].y, scale[4].z);
    scene.add( polygon[4] );
    //список c id объектов фигур, используется при распознавнии клика
    var uuidArr = {
        0: polygon[0].uuid,
        1: polygon[1].uuid,
        2: polygon[2].uuid,
        3: polygon[3].uuid,
        4: polygon[4].uuid
    };
    uuidArr[polygon[0].uuid] = 0;
    uuidArr[polygon[1].uuid] = 1;
    uuidArr[polygon[2].uuid] = 2;
    uuidArr[polygon[3].uuid] = 3;
    uuidArr[polygon[4].uuid] = 4;
    //////

    // цвет для второго многоульника
    var materials = [
        new THREE.MeshNormalMaterial()
        // new THREE.MeshPhongMaterial({ map: texture })
    ];

    // добавляем второй многоугольник в сцену
    var mesh = THREE.SceneUtils.createMultiMaterialObject(geom, materials);
    mesh.children.forEach(function(e) {e.castShadow=true});
    // scene.add(mesh);



    // пользовательский ui
    //var controls = new function () {
    //    this.rotationSpeed = 0.02;
    //
    //    this.scaleX = 1;
    //    this.scaleY = 1;
    //    this.scaleZ = 1;
    //
    //    this.positionX = 0;
    //    this.positionY = 4;
    //    this.positionZ = 0;
    //
    //    this.color = meshMaterial.color.getStyle();
    //
    //    // ф-ия добавления многоульников различный размеров
    //    this.addCube = function () {
    //        var vertices = [
    //            new THREE.Vector3(6,0,0),
    //            new THREE.Vector3(-6,0,0),
    //            new THREE.Vector3(0,Math.ceil((Math.random() * 12)),0),
    //            new THREE.Vector3(0,-Math.ceil((Math.random() * 12)),0),
    //            new THREE.Vector3(0,0,6),
    //            new THREE.Vector3(0,0,-6)
    //        ];
    //
    //        var meshAdd = THREE.SceneUtils.createMultiMaterialObject(geom, [meshMaterial, wireFrameMat]);
    //        meshAdd.position.x = -30 + Math.round((Math.random() * planeGeometry.parameters.width));
    //        meshAdd.position.y = Math.round((Math.random() * 5));
    //        meshAdd.position.z = -20 + Math.round((Math.random() * planeGeometry.parameters.height));
    //
    //        scene.add(meshAdd);
    //    };
    //};
    //
    //// задаем дефолтные значения в ui
    //var gui = new dat.GUI();
    //gui.add(controls, 'rotationSpeed', 0, 0.5);
    //
    //guiScale = gui.addFolder('scale');
    //guiScale.add(controls, 'scaleX', 0, 5);
    //guiScale.add(controls, 'scaleY', 0, 5);
    //guiScale.add(controls, 'scaleZ', 0, 5);
    //
    //guiPosition = gui.addFolder('position');
    //var contX = guiPosition.add(controls, 'positionX', -10, 10);
    //var contY = guiPosition.add(controls, 'positionY', -14, 20);
    //var contZ = guiPosition.add(controls, 'positionZ', -10, 10);
    //
    //gui.add(controls, 'addCube');
    //
    //contX.listen();
    //contX.onChange(function (value) {
    //    meshColor.position.x = controls.positionX;
    //});
    //
    //contY.listen();
    //contY.onChange(function (value) {
    //    meshColor.position.y = controls.positionY;
    //});
    //
    //contZ.listen();
    //contZ.onChange(function (value) {
    //    meshColor.position.z = controls.positionZ;
    //});
    //
    //
    //var spGui = gui.addFolder("Mesh");
    //spGui.addColor(controls, 'color').onChange(function (e) {
    //    meshMaterial.color.setStyle(e)
    //});

    // ф-ия рендера - анимация
    function render() {

        // анимация объектов
        // meshColor.rotation.y += 0.01;
        // meshColor.rotation.x += 0.01;
        // meshColor.rotation.z += 0.01;

        // meshColor.position.y += 0.01;
        // meshColor.position.x += 0.01;
        // meshColor.position.z += 0.01;


        step = 0.04;
        meshColor.position.x = 2 + ( 10 * (Math.cos(step)));
        meshColor.position.y = 2 + ( 10 * Math.abs(Math.sin(step)));

        // анимация объектов - зависимость от ui
        scene.traverse(function (e) {
            if (e instanceof THREE.Mesh && e != plane) {
                e.rotation.x += controls.rotationSpeed;
                e.rotation.y += controls.rotationSpeed;
                e.rotation.z += controls.rotationSpeed;
            }
        });

        // масштаб объектов - зависимость от ui
        meshColor.scale.set(controls.scaleX, controls.scaleY, controls.scaleZ);

        // рендер всех объектов
        requestAnimationFrame(render);
        // setTimeout(requestAnimationFrame(render), 5000);
        renderer.render(scene, camera);
    }

    //смещение, поворот, масштабирование
    function renderXYZ(points, scale, rotation) {
        console.log(scale);
        polygon[0].position.x = points[0].x;
        polygon[0].position.y = points[0].y;
        polygon[0].position.z = points[0].z;
        polygon[0].rotation.x = rotation[0].x;
        polygon[0].rotation.y = rotation[0].y;
        polygon[0].rotation.z = rotation[0].z;
        polygon[0].scale.set(scale[0].x, scale[0].y, scale[0].z);

        polygon[1].position.x = points[1].x;
        polygon[1].position.y = points[1].y;
        polygon[1].position.z = points[1].z;
        polygon[1].rotation.x = rotation[1].x;
        polygon[1].rotation.y = rotation[1].y;
        polygon[1].rotation.z = rotation[1].z;
        polygon[1].scale.set(scale[1].x, scale[1].y, scale[1].z);

        polygon[2].position.x = points[2].x;
        polygon[2].position.y = points[2].y;
        polygon[2].position.z = points[2].z;
        polygon[2].rotation.x = rotation[2].x;
        polygon[2].rotation.y = rotation[2].y;
        polygon[2].rotation.z = rotation[2].z;
        polygon[2].scale.set(scale[2].x, scale[2].y, scale[2].z);

        polygon[3].position.x = points[3].x;
        polygon[3].position.y = points[3].y;
        polygon[3].position.z = points[3].z;
        polygon[3].rotation.x = rotation[3].x;
        polygon[3].rotation.y = rotation[3].y;
        polygon[3].rotation.z = rotation[3].z;
        polygon[3].scale.set(scale[3].x, scale[3].y, scale[3].z);

        polygon[4].position.x = points[4].x;
        polygon[4].position.y = points[4].y;
        polygon[4].position.z = points[4].z;
        polygon[4].rotation.x = rotation[4].x;
        polygon[4].rotation.y = rotation[4].y;
        polygon[4].rotation.z = rotation[4].z;
        polygon[4].scale.set(scale[4].x, scale[4].y, scale[4].z);

        renderer.render(scene, camera);
    }

    function fact(n){
        return (n > 1)?n*fact(n-1):1;
    }

    function ni(n,i){
        return fact(n)/(fact(i)*fact(n-i));
    }

    function bin(n,i,t){
        return ni(n,i)*Math.pow(t,i)*Math.pow(1-t,n-i);
    }

    function bezier(t, maps){
        var sumX = 0, sumY = 0, sumZ = 0;
        for(var i = 0; i < maps.length; i++){
            sumX += maps[i]['x']*bin(maps.length-1,i,t);
            sumY += maps[i]['y']*bin(maps.length-1,i,t);
            sumZ += maps[i]['z']*bin(maps.length-1,i,t);
        }
        return {x: sumX, y: sumY, z: sumZ};
    }

    //t - доля пройденного пути
    //wayArr - путь ввиде массива координат
    //points - массив
    function recountCoordXYZ(t, wayArr, points)
    {
        for(var i = 0; i < 5; i++)
        {
            points[i] = bezier(t, wayArr[i]);
        }
        return points;
    }

    //t - доля пройденного пути
    //rotationArr - массиы с радианами
    //rotations - массив
    function recountRotationXYZ(t, rotationArr, rotations)
    {
        for(var i = 0; i < 5; i++)
        {
            rotations[i] = bezier(t, rotationArr[i]);
        }
        return rotations;
    }

    //t - доля пройденного пути
    //scaleArr - массиы c масштабами
    //scales - массив
    function recountScaleXYZ(t, scaleArr, scales)
    {

        for(var i = 0; i < 5; i++)
        {
            scales[i] = bezier(t, scaleArr[i]);
        }
        return scales;
    }

    //начальные и конечные координаты для 5 сценариев
    var wayArr = setFuguresWay();

    //инициализация пути фигур
    function setFuguresWay()
    {
        var wayArr = [];
        //i - номер сценария //j - номер фигуры
        for(var i = 0; i < 5; i++)
        {
            wayArr[i] = [];
            for(var j = 0; j < 5; j++)
            {
                wayArr[i][j] = [];
                wayArr[i][j].push({x: polygon[j].position.x, y: polygon[j].position.y, z: polygon[j].position.z});
            }
        }
        //для элемента сценария 0
        wayArr[0][0].push({x: -8, y: 0, z: polygon[0].position.z});
        wayArr[0][1].push({x: 30, y: 14, z: 0});
        wayArr[0][2].push({x: -30, y: 14, z: 0});
        wayArr[0][3].push({x: -30, y: -16, z: 0});
        wayArr[0][4].push({x: 30, y: -16, z: 0});
        //для элемента сценария 1
        wayArr[1][0].push({x: 30, y: -16, z: 0});
        wayArr[1][1].push({x: 5, y: 10, z: -20});
        wayArr[1][1].push({x: -8, y: 0, z: polygon[0].position.z});
        wayArr[1][2].push({x: -30, y: 14, z: 0});
        wayArr[1][3].push({x: -30, y: -16, z: 0});
        wayArr[1][4].push({x: 30, y: 15, z: 0});
        //для элемента сценария 2
        wayArr[2][0].push({x: 0, y: 0, z: 10});
        wayArr[2][0].push({x: -30, y: 14, z: 0});
        wayArr[2][1].push({x: 30, y: 14, z: 0});
        wayArr[2][2].push({x: 0, y: 0, z: -30});
        wayArr[2][2].push({x: -8, y: 0, z: polygon[0].position.z});
        wayArr[2][3].push({x: -30, y: -16, z: 0});
        wayArr[2][4].push({x: 30, y: -16, z: 0});
        //для элемента сценария 3
        wayArr[3][0].push({x: 0, y: -16, z: 20});
        wayArr[3][0].push({x: -30, y: -16, z: 0});
        wayArr[3][1].push({x: 30, y: 14, z: 0});
        wayArr[3][2].push({x: -30, y: 14, z: 0});
        wayArr[3][3].push({x: 0, y: 10, z: polygon[0].position.z});
        wayArr[3][3].push({x: -8, y: 0, z: polygon[0].position.z});
        wayArr[3][4].push({x: 30, y: -15, z: 0});
        //для элемента сценария 4
        wayArr[4][0].push({x: 30, y: -16, z: 0});
        wayArr[4][1].push({x: 30, y: 14, z: 0});
        wayArr[4][2].push({x: -30, y: 14, z: 0});
        wayArr[4][3].push({x: -30, y: -16, z: 0});
        wayArr[4][4].push({x: 0, y: 15, z: -25});
        wayArr[4][4].push({x: -8, y: 0, z: polygon[0].position.z});
        return wayArr;
    }

    //начальные и конечные координаты для 5 сценариев
    var scaleArr = setFuguresScale();

    //инициализация пути фигур
    function setFuguresScale()
    {
        var scaleArr = [];
        //i - номер сценария //j - номер фигуры
        for(var i = 0; i < 5; i++)
        {
            scaleArr[i] = [];
            for(var j = 0; j < 5; j++)
            {
                scaleArr[i][j] = [];
                scaleArr[i][j].push({x: polygon[j].scale.x, y: polygon[j].scale.y, z: polygon[j].scale.z});
            }
        }
        //0
        scaleArr[0][0].push({x: 2.6, y: 3.2, z: 2.4});
        scaleArr[0][1].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[0][2].push({x: 0.6, y: 0.6, z: 0.6});
        scaleArr[0][3].push({x: 0.5, y: 0.6, z: 0.5});
        scaleArr[0][4].push({x: 0.5, y: 0.6, z: 0.5});
        //1
        scaleArr[1][0].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[1][1].push({x: 2.6, y: 3.2, z: 2.4});
        scaleArr[1][2].push({x: 0.6, y: 0.6, z: 0.6});
        scaleArr[1][3].push({x: 0.5, y: 0.6, z: 0.5});
        scaleArr[1][4].push({x: 0.5, y: 0.6, z: 0.5});
        //2
        scaleArr[2][0].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[2][1].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[2][2].push({x: 2.6, y: 3.2, z: 2.4});
        scaleArr[2][3].push({x: 0.5, y: 0.6, z: 0.5});
        scaleArr[2][4].push({x: 0.5, y: 0.6, z: 0.5});
        //3
        scaleArr[3][0].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[3][1].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[3][2].push({x: 0.6, y: 0.6, z: 0.6});
        scaleArr[3][3].push({x: 2.6, y: 3.2, z: 2.4});
        scaleArr[3][4].push({x: 0.5, y: 0.6, z: 0.5});
        //0
        scaleArr[4][0].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[4][1].push({x: 0.6, y: 0.6, z: 0.5});
        scaleArr[4][2].push({x: 0.6, y: 0.6, z: 0.6});
        scaleArr[4][3].push({x: 0.5, y: 0.6, z: 0.5});
        scaleArr[4][4].push({x: 2.6, y: 3.2, z: 2.4});
        return scaleArr;
    }

    //начальные и конечные радианы поворота
    var rotationArr = setFigureRotation();
    //инициализация поворотов фигур
    function setFigureRotation()
    {
        var rotationArr = [];
        //i - номер сценария //j - номер фигуры
        for(var i = 0; i < 5; i++)
        {
            rotationArr[i] = [];
            for(var j = 0; j < 5; j++)
            {
                rotationArr[i][j] = [];
                rotationArr[i][j].push({x: polygon[j].rotation.x, y: polygon[j].rotation.y, z: polygon[j].rotation.z});
            }
        }
        //0
        rotationArr[0][0].push({x: -0.3, y: 0, z: 1.765});
        rotationArr[0][1].push({x: 0.05, y: 0, z: 2.0});
        rotationArr[0][2].push({x: 0, y: 0.15, z: 4.25});
        rotationArr[0][3].push({x: -0.5, y: 0.8, z: 3.05});
        rotationArr[0][4].push({x: -0.4, y: -0.9, z: 3.95});
        //1
        rotationArr[1][0].push({x: -0.4, y: -0.9, z: 3.95});
        rotationArr[1][1].push({x: -0.3, y: 0, z: 1.765});
        rotationArr[1][2].push({x: 0, y: 0.15, z: 4.25});
        rotationArr[1][3].push({x: -0.5, y: 0.8, z: 3.05});
        rotationArr[1][4].push({x: 0.05, y: 0, z: 2.0});
        //2
        rotationArr[2][0].push({x: -0.3, y: 0, z: 1.765});
        rotationArr[2][1].push({x: 0.05, y: 0, z: 2.0});
        rotationArr[2][2].push({x: -0.3, y: 0, z: 1.765});
        rotationArr[2][3].push({x: -0.5, y: 0.8, z: 3.05});
        rotationArr[2][4].push({x: -0.4, y: -0.9, z: 3.95});
        //3
        rotationArr[3][0].push({x: -0.5, y: 0.8, z: 3.05});
        rotationArr[3][1].push({x: 0.05, y: 0, z: 2.0});
        rotationArr[3][2].push({x: 0, y: 0.15, z: 4.25});
        rotationArr[3][3].push({x: -0.3, y: 0, z: 1.765});
        rotationArr[3][4].push({x: -0.4, y: -0.9, z: 3.95});
        //4
        rotationArr[4][0].push({x: -0.4, y: -0.9, z: 3.95});
        rotationArr[4][1].push({x: 0.05, y: 0, z: 2.0});
        rotationArr[4][2].push({x: 0, y: 0.15, z: 4.25});
        rotationArr[4][3].push({x: -0.5, y: 0.8, z: 3.05});
        rotationArr[4][4].push({x: -0.3, y: 0, z: 1.765});
        return rotationArr;
    }

    //запуск анимации
    //wayArr - путь в формате [{x:0, y:0},{x:0, y:0},...]
    function startAnimation(wayArr, rotationArr, scaleArr)
    {
        var step = 0.015;//скорость
        var t = 0.0;
        var points = [];
        var rotations = [];
        var localScales = [];
        var requestAnimationId = requestAnimationFrame(function animateMove(time){
            localScales = recountScaleXYZ(t, scaleArr, localScales);
            points = recountCoordXYZ(t, wayArr, points);
            rotations = recountRotationXYZ(t, rotationArr, rotations);
            renderXYZ(points, localScales, rotations);
            //свиг вправо освещения
            spotLight.position.x += 1;
            t += step;
            if(t >= 1)//условие остановки анимации
            {
                cancelAnimationFrame(requestAnimationId);
            }
            else
            {
                requestAnimationFrame(animateMove);
            }
        });
    }

    var raycaster = new THREE.Raycaster();
    var mouseVector = new THREE.Vector3();

    //добавление события на клик мыши по canvas
    this.initMouseEvent = function()
    {
        document.getElementsByTagName('canvas')[0].addEventListener("click", function(event){
            var scenario = getFigure(event);
            console.log(scenario, wayArr[scenario], rotationArr[scenario]);
            startAnimation(wayArr[scenario], rotationArr[scenario], scaleArr[scenario]);
        });
    }

    //получение номера объекта, над которыми кликнули мышью и который ближе всех
    function getFigure(event)
    {
        mouseVector.x = 2 * (event.clientX / width) - 1;
        mouseVector.y = 1 - 2 * (event.clientY / height );
        raycaster.setFromCamera(mouseVector.clone(), camera);
        var intersects = raycaster.intersectObjects( scene.children );
        if(intersects.length > 0)
        {
            return uuidArr[intersects[0].object.uuid];
        }
    }

    renderer.render(scene, camera);

    this.start = function()
    {
        startAnimation(wayArr[0], scale);
    }
}

var renderObject;

window.onload = createCanvas;
function createCanvas()
{
    renderObject = new localRenderObject();
    renderObject.initMouseEvent();
};
