
//pageslider(document.getElementById('wrap'), function(){
//});

$(window).on('load', function () {
    // loader
    var $preloader = $('#loader'),
        $spinner   = $preloader.find('.circle');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});

$(document).ready(function() {

    // main slider
    function pageSlider(mainSlide, navSlide) {
        $(mainSlide).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            //fade: true,
            asNavFor: navSlide
        });
        $(navSlide).slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: mainSlide,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true
        });
    }
    pageSlider('.slider-for-1', '.slider-nav-1');
    pageSlider('.slider-for-2', '.slider-nav-2');
    pageSlider('.slider-for-3', '.slider-nav-3');
    pageSlider('.slider-for-4', '.slider-nav-4');
    pageSlider('.slider-for-5', '.slider-nav-5');


    // fullpage slider
    $('#fullpage').fullpage({
        //sectionsColor: ['../images/mainImg-2.jpg', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000']
        anchors: ['firstPage', '3DPage', 'secondPage', '3rdPage', '4thPage', '5Page', '6Page', 'lastPage'],
        menu: '#menu',
        css3: true
        //scrollingSpeed: 1000,
        //scrollBar: true
    });

    $('.burgerIco a').click(function() {
        $('.burgerMenu').slideToggle();
        return false;
    });
    $('.slider-nav .slick-slide a').click(function() {
        return false;
    });

    // popover
    $('[rel="popover"]').popover({
        trigger: 'hover'
    });

    // modal
    $(".item").each(function() {
        $(this).mouseover(function() {
            $("#modal-id").modal('show');
        });
        $(this).mouseleave(function() {
            $("#modal-id").modal('hide');
        });
    });


    // video iframe
    //$(".view").click(function(){
    //    document.getElementById("main").play();
    //    return false;
    //});



    //var BV;
    //$(function() {
    //
    //    BV = new $.BigVideo();
    //    BV.init();
    //
    //    BV.show('video/shot_2and3.mp4');
    //    //BV.show('images/background.jpg');
    //
    //
    //    // Video Play/Pause toggle
    //    $('#video-toggle').on('click', function(e) {
    //        if (this.checked)   BV.getPlayer().play();
    //        else                BV.getPlayer().pause();
    //    });
    //});
    function videoDown() {
        $('main').css('opacity', '1');
    }
    $("#myVideo").bind("ended",videoDown);
});

$(window).scroll(function() {
    // dynamic navbar-main
    var top = $(document).scrollTop();
    if (top > 200) $('.navbarMain').addClass('hide');
    else $('.navbarMain').removeClass('hide');
});




//var videoSource = new Array();
//videoSource[0]='video/shot_1_v2_hi_quality.mp4';
//videoSource[1]='video/shot_2and3_hi_quality_1_vikhod.mp4';
//videoSource[2]='video/shot_2and3_hi_quality_1_loop.mp4';
//var videoCount = videoSource.length;
//var i = 0;
//document.getElementById("myVideo").setAttribute("src",videoSource[0]);
//
//function videoPlay(videoNum) {
//    document.getElementById("myVideo").setAttribute("src",videoSource[videoNum]);
//    document.getElementById("myVideo").load();
//    document.getElementById("myVideo").play();
//}
//
//
//document.getElementById('myVideo').addEventListener('ended',myHandler,false);
//function myHandler() {
//    //for (i = 0; i < 3; i++) {
//    //    videoPlay(i);
//    //}
//    i++;
//    if(i == (videoCount+1)){
//        i = 0;
//        videoPlay(i);
//    }
//    else if (i == 2) {
//        document.getElementById('myVideo').style.display = 'none';
//        document.getElementById('myVideoLoop').style.display = 'block';
//    }
//    else {
//        videoPlay(i);
//    }
//
//}