jQuery(document).ready(function(event){
	var projectsContainer = $('.cd-projects-container'),
		navigation = $('.cd-primary-nav'),
		triggerNav = $('.cd-nav-trigger'),
//		id = $('.single-project').attr("href"),
		logo = $('.cd-logo'),
		main = $('.main-container'),
		footer = $('.footer-container');
	


	projectsContainer.on('click', '.single-project', function(){
//        var id = $(this).attr("id");
		var selectedProject = $(this);
		if( projectsContainer.hasClass('nav-open') ) {
			//close navigation
			triggerNav.add(projectsContainer).add(navigation).removeClass('nav-open');
//            triggerNav.attr("href", '#');
		} else {
			//open project
			selectedProject.addClass('selected');
			projectsContainer.add(triggerNav).add(logo).addClass('project-open').add(main).addClass('project-open').add(footer).addClass('project-open');
            triggerNav.addClass('anchor').attr("href", '#main');
		}
	});

	projectsContainer.on('click', '.cd-scroll', function(){
		//scroll down when clicking on the .cd-scroll arrow
		var visibleProjectContent =  projectsContainer.find('.selected').children('.cd-project-info'),
			windowHeight = $(window).height();

		visibleProjectContent.animate({'scrollTop': windowHeight}, 300); 
	});
    
    triggerNav.on('click', function(){
		if( triggerNav.hasClass('project-open') ) {
			//close project
			projectsContainer.removeClass('project-open').find('.selected').removeClass('selected').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$(this).children('.cd-project-info').scrollTop(0).removeClass('has-boxshadow');

			});
			triggerNav.add(logo).removeClass('project-open').add(main).removeClass('project-open').add(footer).removeClass('project-open');
            setTimeout(function(){triggerNav.removeClass('anchor').attr("href", '#'); }, 500);
		} else {
			//trigger navigation visibility
			triggerNav.add(navigation).toggleClass('nav-open');
            return false;
		}
	});

	//add/remove the .has-boxshadow to the project content while scrolling 
	var scrolling = false;
	projectsContainer.find('.cd-project-info').on('scroll', function(){
		if( !scrolling ) {
		 	(!window.requestAnimationFrame) ? setTimeout(updateProjectContent, 300) : window.requestAnimationFrame(updateProjectContent);
		 	scrolling = true;
		}
	});

	function updateProjectContent() {
		var visibleProject = projectsContainer.find('.selected').children('.cd-project-info'),
			scrollTop = visibleProject.scrollTop();
		( scrollTop > 0 ) ? visibleProject.addClass('has-boxshadow') : visibleProject.removeClass('has-boxshadow');
		scrolling = false;
	}
    
    function alturaMaxima() {
        var altura = $(window).height();
        $(".full-screen").css('height', altura);
    }
    alturaMaxima();
    $(window).bind('resize', alturaMaxima);

    
    
    $('.lineSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
        //fade: true,
    });
    
        
     $('a.anchor').bind("click", function(e){
          var anchor = $(this);
          $('html, body').stop().animate({
             scrollTop: $(anchor.attr('href')).offset().top
          }, 500);
          e.preventDefault();
    });
    return false;
});