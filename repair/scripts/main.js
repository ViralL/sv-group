'use strict';
function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

$(window).on('load', function () {
    var $preloader = $('#preloader'),
        $spinner = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
    $('body').removeClass('loader');
});

$(document).ready(function () {

    // mask phone
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // input[type=file]
    var wrapper = $('<div/>').css({ height: 0, width: 0, 'overflow': 'hidden' });
    var fileInput = $(':file').wrap(wrapper);
    fileInput.change(function () {
        //var item = $(this);
        $('.file_btn').text('Загружено');
    });
    $('.file_btn').click(function () {
        fileInput.click();
        return false;
    }).show();
    // input[type=file]

    // anchor
    $(".anchor").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top - 0 }, 1000);
    });
    // anchor

    $('.mainMenuBtn').click(function () {
        $(this).parents().find('.mainMenu').css('left', '0');
    });

    $('.mainMenuClose').click(function () {
        $(this).parent().css('left', '-100%');
    });

    // input type ONLY number
    jQuery.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // sucess backspace, tab, delete, arrows, numbers
                return key == 8 || key == 9 || key == 46 || key == 190 || key >= 37 && key <= 40 || key >= 48 && key <= 57 || key >= 96 && key <= 105;
            });
        });
    };
    $(".input__number-js").ForceNumericOnly();
    // input type ONLY number

    $('.chart').appear(function () {
        $('.chart.termDiag__item-succ').easyPieChart({
            easing: 'easeOutBounce',
            scaleLength: 0,
            size: 142,
            lineWidth: 5,
            barColor: '#82ca9c',
            trackColor: '#ebebeb',
            onStep: function onStep(from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        $('.chart.termDiag__item-info').easyPieChart({
            easing: 'easeOutBounce',
            scaleLength: 0,
            size: 142,
            lineWidth: 5,
            barColor: '#6dcff6',
            trackColor: '#ebebeb',
            onStep: function onStep(from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        $('.chart.termDiag__item-dang').easyPieChart({
            easing: 'easeOutBounce',
            scaleLength: 0,
            size: 142,
            lineWidth: 5,
            barColor: '#f00',
            trackColor: '#ebebeb',
            onStep: function onStep(from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        $('.chart.termDiag__item-def').easyPieChart({
            easing: 'easeOutBounce',
            scaleLength: 0,
            size: 351,
            lineWidth: 8,
            barColor: '#ebebeb',
            trackColor: 'transparent'
        });
        //var chart = window.chart = $('.chart').data('easyPieChart');
    });

    // animation number
    $('.number').appear(function () {
        $('.number').each(function () {
            var dataN = $(this).attr('data-number');
            $(this).animate({ num: dataN }, {
                duration: 3000,
                step: function step(num) {
                    this.innerHTML = num.toFixed(0);
                }
            });
        });
    });
    $('.number2').appear(function () {
        $('.number2').each(function () {
            var dataN = $(this).attr('data-number');
            $(this).animate({ num: dataN }, {
                duration: 1100,
                step: function step(num) {
                    this.innerHTML = num.toFixed(0);
                }
            });
        });
    });

    //
    //function move() {
    //    $(".animate")
    //        .animate({left: "783", top: '22', left: '791'}, 2500)
    //        //.animate({top: '22', left: '791'}, 2500)
    //        //.animate({top: '40', left: '797'}, 2500)
    //        //.animate({top: '50'}, 2500)
    //        //.animate({left: "44"}, 2500)
    //        //.animate({top: '310'}, 2500)
    //        //.animate({left: "774"}, 2500)
    //}
    //
    //function mentmove() {
    //    setInterval(move, 3500);
    //}
    //move();

    // owl slider
    var $sync1 = $(".big-images"),
        $sync2 = $(".thumbs"),
        flag = false,
        duration = 300;
    $sync1.owlCarousel({
        items: 1,
        margin: 10,
        nav: false,
        loop: true,
        dots: false,
        navText: ["<i class='icon-left-open-big'></i>", "<i class='icon-right-open-big'></i>"],
        responsive: {
            769: {
                nav: true
            }
        }
    }).on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });
    $sync2.owlCarousel({
        margin: 20,
        items: 6,
        nav: false,
        center: false,
        dots: false,
        //loop: true,
        mouseDrag: false
    }).on('click', '.owl-item', function () {
        $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    }).on('changed.owl.carousel', function (e) {
        if (!flag) {
            flag = true;
            $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
            flag = false;
        }
    });
    $(function () {
        $('.thumbs').on('click', '.owl-item', function () {
            $(this).addClass('main').siblings().removeClass('main');
            //return false;
        });
    });
    $(".mobile-slider").owlCarousel({
        margin: 0,
        items: 1,
        nav: true,
        dots: false,
        navText: ["", "<i class='icon-right-open-big'></i>"],
        loop: true
    });
    // $('.projectsInner a').on('click', function () {
        // setInterval(function () {
        //    $(".mainSlider").owlCarousel({
        //        margin: 0,
        //        items: 1,
        //        nav: true,
        //        mouseDrag : false,
        //        dots: false,
        //        navText: ["<img src='images/icon-left.png'>", "<img src='images/icon-right.png'>"],
        //        loop: true
        //    });
            $(".mainSlider").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true
                //fade: true,
            });
            var $sync3 = $(".big-images1"),
                $sync4 = $(".thumbs1"),
                flag = false,
                duration = 300;
            $sync3.owlCarousel({
                items: 1,
                margin: 10,
                nav: false,
                loop: true,
                dots: false
            }).on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;
                    $sync4.trigger('to.owl.carousel', [e.item.index, duration, true]);
                    flag = false;
                }
            });
            $sync4.owlCarousel({
                margin: 20,
                items: 4,
                nav: true,
                center: false,
                dots: false,
                navText: ["<i class='icon-left-open-big'></i>", "<i class='icon-right-open-big'></i>"],
                //loop: true,
                mouseDrag: false
            }).on('click', '.owl-item', function () {
                $sync3.trigger('to.owl.carousel', [$(this).index(), duration, true]);
            }).on('changed.owl.carousel', function (e) {
                if (!flag) {
                    flag = true;
                    $sync3.trigger('to.owl.carousel', [e.item.index, duration, true]);
                    flag = false;
                }
            });
            $(function () {
                $('.thumbs').on('click', '.owl-item', function () {
                    $(this).addClass('main').siblings().removeClass('main');
                    //return false;
                });
            });
    //     }, 100);
    // });
    // owl slider

    // calc
    function count_totals() {
        var summ1, summ2, summ3, summ4 = 0;
        //var type = $('.cart_item-type').val();
        //var style = $('.cart_item-style').val();
        //var room = $('.cart_item-room').val();
        var count = $('.cart_item-count').val();
        summ1 = 7500 * count;
        summ2 = 5000 * count;
        summ3 = summ1 + summ2;

        //console.log(summ);

        $(".calcSumm__size").text(count);
        $(".calcSumm__mat").text(summ2);
        $(".calcSumm__main").text(summ3);
        $(".calcSumm__work").text(summ1);
    }
    $(".cart_item select, .cart_item input").on("change", function () {
        number_format(count_totals(), 0, ' ', ' ')
    });
    // calc
});
//# sourceMappingURL=main.js.map