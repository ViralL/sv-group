
$('#ball').appear(function() {
    var ball = document.getElementById("ball");
    var source1 = [
        65, 65, 65,
        65, 65, 65,
        65, 65, 65,
        65, 65, 774,
        774, 793, 793,
        793, 793, 793,
        793, 793, 793,

        799, 799, 806, 811,
        815, 815, 809, 809,

        809, 809, 809,
        809, 809, 809,
        809, 809, 809,

        776, 776, 776, 776,
        776, 776, 776, 776,
        776, 776, 776, 776,
        41, 41, 41, 41,
        41, 41, 41, 41,
        41, 41, 41, 41,
        31, 24, 19, 14,

        12, 10, 6, 3,
        2, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 2, 5, 10,
        16, 23, 40, 40,

        40, 40, 40, 40,
        40, 40, 40, 40,
        40, 40, 40, 40,
        40, 40, 774, 774];
    var source2 = [
        -2, -2, -2,
        -2, -2, -2,
        -2, -2, -2,
        -2, -2, -2,
        -2, -2, -2,
        -2, -2, -2,
        -2, -2, -2,

        5, 10, 20, 30,
        40, 50, 60, 70,

        75, 80, 85,
        90, 95, 100,
        143, 143, 143,

        143, 143, 143, 143,
        143, 143, 143, 143,
        143, 143, 143, 143,
        143, 143, 143, 143,
        143, 143, 143, 143,
        143, 143, 143, 143,
        150, 155, 160, 165,

        170, 175, 180, 185,
        190, 195, 200, 205,
        210, 215, 220, 225,
        230, 235, 240, 245,
        250, 260, 270, 280,
        290, 300, 310, 310,

        310, 310, 310, 310,
        310, 310, 310, 310,
        310, 310, 310, 310,
        310, 310, 310, 310];
    var maps = [];
    for (var i = 0, j = 0; i < source1.length; i++, j++) {
        maps[j] = {};
        maps[j]["x"] = source1[i];
        maps[j]["y"] = source2[i];
    }
    function fact(n) {
        return n > 1 ? n * fact(n - 1) : 1;
    }
    console.log("Факториал 5 = ", fact(5));

    function ni(n, i) {
        return fact(n) / (fact(i) * fact(n - i));
    }
    function bin(n, i, t) {
        return ni(n, i) * Math.pow(t, i) * Math.pow(1 - t, n - i);
    }
    function bezier(t, maps) {
        var res = { x: 0, y: 0 },
            sumX = 0,
            sumY = 0;
        for (var i = 0; i < maps.length; i++) {
            sumX += maps[i]["x"] * bin(maps.length - 1, i, t);
            sumY += maps[i]["y"] * bin(maps.length - 1, i, t);
        }
        return { x: sumX, y: sumY };
    }
    var point = {},
        t = 0,
        step = 0.02;
    (function () {
        if (t < 1) {
            t += step;
            point = bezier(t, maps);

            ball.style.left = point["x"] + "px";
            ball.style.top = point["y"] + "px";
            setTimeout(arguments.callee, 100);
        } else console.log("работа завершена");
    })();

});